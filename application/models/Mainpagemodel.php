<?php

class Mainpagemodel extends CI_Model {
 
  /*  public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }
*/
    public function getuserdata($id='')
    {
            $query = $this->db->where('user_id',$id)->get('pan_userdetails_tbl');
            return $query->result();
    }

    public function loginuser($var='')
    {
        //return $var;
        $data = array(
        'att_userid'=>$var,
        'login_time'=>date('Y-m-d H:i:s')
        );
        return $this->db->insert('pan_attendence_tbl',$data);
    }

    public function getUserattendence($id)
    {
        $query = $this->db->where('att_userid',$id)->get('pan_attendence_tbl');
        return $query->result();
    }

    public function logoutuser($var='')
    {
        //return $var;
        $data = array(
        'logout_time'=>date('Y-m-d H:i:s')
        );
        return $this->db->where('att_userid',$var)->update('pan_attendence_tbl',$data);
        //$this->db->update($table_name, array('title' => $title));
    }

    public function getuserdatainfoModel($id)
    {
        $query = $this->db->where('att_userid',$id)->get('pan_attendence_tbl');
        return $query->result();

    }

    public function getTodaydata($id)
    {   
        $start = date("Y-m-d")." 00:00:00";
        $end = date("Y-m-d")." 23:59:59";
        
        $query = $this->db->where('att_userid',$id)->where('login_time >=', $start)->where('login_time <=', $end)->get('pan_attendence_tbl');
        return $query->row_array();
    }

    public function getUserDetails_model($id)
    {
       $query = $this->db->where('user_id',$id)->get('pan_userdetails_tbl');
        return $query->row_array();
    }
}

?>