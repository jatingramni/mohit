<?php

class Userpagemodel extends CI_Model {

    public function getuserdata($id='')
    {
            $query = $this->db->where('user_id',$id)->get('pan_userdetails_tbl');
            return $query->result();
    }

    public function loginuser($var='')
    {
        //return $var;
        $data = array(
        'att_userid'=>$var,
        'login_time'=>date('Y-m-d h:i:s')
        );
        return $this->db->insert('pan_attendence_tbl',$data);
    }

    public function getUserattendence($id)
    {
        $query = $this->db->where('att_userid',$id)->get('pan_attendence_tbl');
        return $query->result();
    }

    public function logoutuser($var='')
    {
        //return $var;
        $data = array(
        'logout_time'=>date('Y-m-d h:i:s')
        );
        return $this->db->where('att_userid',$var)->update('pan_attendence_tbl',$data);
        //$this->db->update($table_name, array('title' => $title));
    }
}

?>