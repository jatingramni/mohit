<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userpage extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('userpagemodel');
        // Your own constructor code
    }
    public function index()
    {	
        $data['user']= $this->userpagemodel->getuserdata($this->session->userdata('user_id'));
        $data['attendence'] = $this->userpagemodel->getUserattendence($this->session->userdata('user_id'));
        $this->load->view('testpage',$data);	
    }

    public function logincontrol()
    {
        echo $this->userpagemodel->loginuser($_POST['user_id']);
    }

    public function logoutcontrol()
    {
        echo $this->userpagemodel->logoutuser($_POST['user_id']);
    }

    public function FunctionName($value='')
    {
        $templ = file_get_contents('index.html');
        $variable = array('title'=>"Hey jatin Gramni");

        foreach ($variable as $key => $value) {
            str_replace("{{".$key."}}", $value, $templ);
        }

        $('#id').css({'background-color':'red','color':'#fff'});
    }


}
