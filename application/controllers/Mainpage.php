<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpage extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mainpagemodel');
        // Your own constructor code
    }
	public function index()
    {	
        if ($this->session->userdata('user_id')=="") {
            redirect(base_url());
        }else{
            $data['userdata'] = $this->gettodaydata($this->session->userdata('user_id')); 
            $data['userDetails'] = $this->getUserDetails($this->session->userdata('user_id')); 
            $this->load->view('header',$data);
            $this->load->view('mainpage',$data);
            $this->load->view('footer');
        }
        
	}

    public function getUserDetails($id='')
    {

        $data = $this->mainpagemodel->getUserDetails_model($id);
        return $data;
    }

    public function gettodaydata($id='')
    {

        $data = $this->mainpagemodel->getTodaydata($id);
        return $data;
    }

    public function getuserdatainfo()
    {
        $data =  $this->mainpagemodel->getuserdatainfoModel($this->session->userdata('user_id'));
        $response = array();
        foreach ($data as $key) {
            $temp['title'] = $this->createCalenderTitle($key->login_time,$key->logout_time);
            //$temp['end'] = $key->logout_time;
            $temp['start'] = $key->login_time;

            $response[] = $temp;
        }
        echo json_encode($response);

    }

    public function createCalenderTitle($startDate='',$endDate='')
    {
        if ($startDate!='' && $endDate!='') {
            $date ="";
            if ($endDate =='0000-00-00 00:00:00') {
                $data =$this->dateFormat($startDate,'h:i a');
            }else{
                 $data =$this->dateFormat($startDate,'h:i a')."-".$this->dateFormat($endDate,'h:i a')."\nTotal time : ".$this->timeDiff($startDate,$endDate);//.date_diff($endDate-$startDate); 
            }
           
            return $data;
        }

        
    }

    public function dateFormat($date,$format)
    {
        $datenew = new DateTime($date);
        return $datenew->format($format);
    }

    public function timeDiff($startDate='',$endDate='')
    {
        $start_date = new DateTime($startDate);
        $since_start = $start_date->diff(new DateTime($endDate));
        if ($since_start->h!=0) {
            return $since_start->h.'Hr :'.$since_start->i.' Min';
        }else{
            return $since_start->i.' Min';
        }
        
    }
    public function logincontrol()
    {
        echo $this->mainpagemodel->loginuser($_POST['user_id']);
    }

    public function logoutcontrol()
    {
        echo $this->mainpagemodel->logoutuser($_POST['user_id']);
    }

    public function logout()
    {
        $this->session->unset_userdata('user_id');
        redirect(base_url());
    }

}
