<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
		{
			parent::__construct();
			$this->load->model('homemodel');
			// Your own constructor code
		}
	public function index()
	{	
		$this->load->view('home');
	}

	public function signin()
	{
		$my_model = $this->homemodel->sign();
		if (count($my_model)>0) {
			$the_session = array("user_id" => $my_model['user_id']);
			$this->session->set_userdata($the_session);
			redirect('mainpage');
		}else{
			redirect('welcome');
		}
	}
}
