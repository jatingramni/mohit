$("#signin" ).click(function() {
    $.ajax({
        url: 'mainpage/logincontrol',
        data: {'user_id': $('#userid').val()}, // change this to send js object
        type: "post",
        success: function(data){
           //document.write(data); just do not use document.write
           console.log(data);
           location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
         }
      });
});

$("#signout" ).click(function() {
  if (confirm('Are You Sure?')){
   $.ajax({
        url: 'mainpage/logoutcontrol',
        data: {'user_id': $('#userid').val()}, // change this to send js object
        type: "post",
        success: function(data){
           //document.write(data); just do not use document.write
           console.log(data);
           location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
         }
      });
  }else{
   alert("You are not exit till now.")
  }
    
});